<?php

namespace App\Tests\Unit;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
    }

    public function testGetFirstName(): void
    {
        $value = "Anthony";

        $response = $this->user->setFirstName($value);
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getFirstName());
    }

    public function testGetLastName(): void
    {
        $value = "reyre";

        $response = $this->user->setLastName($value);
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getLastName());
    }

    public function testGetPhone(): void
    {
        $value = "06.37.20.32.99";

        $response = $this->user->setPhone($value);
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPhone());
    }

    public function testGetMail(): void
    {
        $value = "test@test.fr";

        $response = $this->user->setMail($value);
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getMail());
    }
}